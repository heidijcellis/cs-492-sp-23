<small>*Version 2023-Spring-1.0, Revised Sat Jan 07 2023*</small>

# Sprint Evaluation

Each sprint is evaluated in the following areas:

- 30% Planning - Team and individual scores
- 45% Review - Team and individual scores
- 25% Retrospective - Individual score only

## 1. Planning 40% (team & individual)

In the week preceding the Sprint Planning meeting, you will preparing for the planning meeting that takes place in class.

You and your team will be graded on what you have done in the preceding week to help prepare for the planning meeting.

### 1.1 Team Score

Your team will be assigned a score at the end of the Sprint Planning Meeting that indicates how well the team has planned for the upcoming sprint. This score will be determined by the instructor based on the issues that are placed in the board's To Do column for the sprint. The following rubric will be used:

Description | Score
--- | ---
Clearly enough work planned for the upcoming sprint | 100%
Maybe enough work planned for the upcoming sprint (but not clear) | 75%
Clearly not enough work planned for the upcoming sprint | 50%
No work planned for the upcoming sprint | 0%

"Enough" is a function of the team size, the length of the sprint, and the weight of the issues accepted into To-Do. For a more detailed example, assume your class meets twice a week, the sprint being planned has 4 work sessions (i.e., 2-weeks), and the team has 5 members. Assuming each issue accepted into To-Do is approximately 1-person-weeks worth of effort, we should expect around 10 issues, and this is probably a bit of an overestimate. So 9 or more is clearly enough, 5 or less (assuming the same approximate weight) is clearly not enough, and in between is unclear. Ultimately, it is your instructor's judgement as to where to draw the line. **It is critically imporant that each team member complete activities in GitLab!!  In other words, do not work in a group with one person making all changes to GitLab.**

Example activities to complete while preparing for Sprint Planning:
*	Comment on existing issues to help refine them.		
*	Create new issues for missing items.
*	Identify issues that you think should be closed, and explain why.
*	Ask or answer clarifying questions on an issue.			
*	Suggest a weight for an issue, and explain why.			
*	Suggest that an issue should be in the next sprint and why.	
*	Propose who should do something and explain why.			
*	Document resources useful for completing a task.		
*	Provide a plan, checklist, or list of steps for completing a task.	
*	Apply or remove labels appropriately, and explain why.			
*	Respond to someone else's comment (emojis are acceptable).

In general, you should try to plan a bit more than enough. During the planning meeting, if too much is accepted in To-Do after the "Proposed" has been processed, then the team and the PO, should prioritize the To-Do, and then move low priority items to the "Backlog". These can then be used in future sprints, assuming they are still relevant, which may help reduce the team's planning
burden for the next sprint.

### 1.2 Individual Score

You will be assigned a score after the Sprint Planning Meeting that indicates how well you helped the team prepare for the planning meeting. This score will be determined by your team evaluation using the following multiplier:

Score | Multiplier
--- | ---
5 | 1.15
4 | 1
3 | 0.85
2 | 0.7
1 | 0.55

Note that I will use a multiplier that scales the scores to the correct multiplier (e.g., a 3.4 Catme score results in a 0.91 multiplier).

## 2. Review 40% (team)

During and immediately after the Sprint Review meeting, you will be evaluated ***as a team*** on:

* 50% Sufficient progress.
* 50% Effective use of GitLab to coordinate the team.


### 2.1 Sufficient progress

This will be determined by the acceptance criteria cooperatively set with the Product Owner during Sprint Planning. Acceptance criteria can be renegotiated with the Product Owner, with suitable justification, at least one week before the Sprint Review.

Percentage of To-Do Items Completed | Score
--- | ---
&geq; 75% | 100%
&geq; 50% | 75%
&geq; 25% | 50%
&lt; 25% | 0%


### 2.2 Effective use of GitLab to coordinate the team

Description | Score
--- | ---
Clearly good enough | 100%
Could be improved | 75%
Clearly not good enough | 50%
GitLab is not be used at all | 0%

**It is critically imporant that each team member complete activities in GitLab!!  In other words, do not work in a group with one person making all changes to GitLab.** Example activities that show the use of GitLab to coordinate the team:
* Move an item on the board.
* Edit issue titles and/or descriptions, and explain why.
* Comment on existing issues to help refine them.		
* Create new issues for missing items.
* Identify issues that you think should be closed, and explain why.			
* Ask or answer clarifying questions on an issue.			
* Assign a weight to an issue, and explain why.			
* Assign a deadline to an issue, and explain why.		
* Assign an issue to someone, and explain why.			
* Document resources useful for completing a task.		
* Provide a plan, checklist, or list of steps for completing a task.		
* Apply or remove labels appropriately, and explain why.			
* Respond to someone else's comment (emojis are acceptable).
* Create a branch.
* Make a commit to a branch.
* Review someone else's code

### 2.3 Individual Score

You will be assigned a score after the Sprint Review Meeting that indicates how well you helped the team complete the tasks for the current sprint. This score will be determined by your team evaluation using the following multiplier:

Score | Multiplier
--- | ---
5 | 1.15
4 | 1
3 | 0.85
2 | 0.7
1 | 0.55

Note that I will use a multiplier that scales the scores to the correct multiplier (e.g., a 3.4 score results in a 0.91 multiplier).

## 3. Retrospective 20% (individual)

At the end of each sprint, you will post a Sprint Retrospective report to a designated issue about your learning in the class, the work and work products you are producing, etc. Your post is expected to contain:

* 500-750 words.
* Identify the Sprint Number (e.g. Sprint-1).
* Links to evidence of activity on GitLab with one sentence description for each link.
* Reflection on what worked well
* Reflection on what didn't work well
* Reflection on what changes could be made to improve as a team
* Reflection on what changes could be made to improve as an individual
* Reflection on how you are performing in your role this sprint
* Due before the date and time of the Sprint Retrospective Meeting.

Description | Score
--- | ---
Sufficient | 100%
Needs improvement | 75%
Lacking significantly in any area | 50%
Non-existent | 0

## Attendance and Participation

Attendance and participation in this class is vital to the success of your team. You are expected to attend every class. In class you will working with your teammates on planning, reviewing, estimating, creating and selecting tasks, learning, and implementing. It is important for all members of the team to be present to enable the team to make progress.

To be consider present and participating for a particular class, you are expected to:

* Be present.
* Arrive on time.
* Participate fully in team activities in a timely fashion. 

**Each of the four teams will have a slightly different schedule for the types of classes listed below.  See the schedule for details.**

Major sessions are Pre-Planning, Sprint Planning, Sprint Review, and Sprint Retrospective. Work sessions are all other class sessions. **An unexcused absence from a major session will result in an additional 10 point sprint grade reduction for that individual.**

## Copyright and License
#### &copy; 2023 Stoney Jackson, Karl Wurst and Heidi Ellis
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA2019
