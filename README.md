# CS 492 - Computer Science Capstone

This class will be run in a fashion similar to a production-grade software project. We will make some minor adjustments due to the fact that developers are students and this class maks up only part of their full-time effort. The goal is for students to gain experience developing a real-world project. For spring 2023, we will front-end load the course with learning about open source and GitLab in order to prepare students to enter the development evironment. We will have three sprints, each approximately 4.5 weeks in length. 

## Expectations
The goal of this course is for you to participate in and contribute to a real-world project. As such: 
 - There will be real-world bumps in the road and real-world constraints
 - This course will be different from any other course you’ve had
 - This may feel uncomfortable and you may not like parts of it
 - However, you will appreciate the course after you have graduated
    - A former student states: “Thank you as well for the confidence that I was able to build during capstone as it really helped me feel capable of real-world planning, development, and conversations in the workplace.”
 - Remember that college education is about learning to learn, and not as much about learning content
    - By now you should not expect me to teach you anything, but to simply guide your self-learning
 - Consider me the Dilbert pointy headed boss
   - I will manage the overall project, but I typically will not know details of technologies
   - You will have to keep me apprised of your progress 
   - Expect me to learn along side of you
 - We will be working in public so remember that everything you do will be visible
   - All grading will be private
   - Feel free to create an anonymous GitLab handle if you're concerned about being visible
   - Put on your professional hat and keep it on at all times, including Discord discussions

The following links provide information about the course and how it is structured. **You are responsible for reading and knowing all information in these links.**

## 1. [Syllabus](https://gitlab.com/heidijcellis/cs-492-sp-23/-/blob/master/Syllabus.MD)
The syllabus allows you to understand what is required with respect to grading and your participation in the course. 

## 2. [Sprint Evaluation](https://gitlab.com/heidijcellis/cs-492-sp-23/-/blob/master/Sprint-Evaluation.MD)
This page provides you with an understanding of how you will work and how you will be graded. 

## 3. [Schedule](https://gitlab.com/heidijcellis/cs-492-sp-23/-/blob/master/Schedule.MD) 
This page provides you with an understanding of what will happen during each week of the semester. This schedule may change during project development.  

## 4. [Projects](https://gitlab.com/heidijcellis/cs-492-sp-23/-/blob/master/Projects.MD) 
This page describes the projects that we'll be working on this semester. 

## 5. [Boards](https://gitlab.com/heidijcellis/cs-492-sp-23/-/blob/master/Boards.MD)
This page describes the issue board that will be used by each team as well as describing how the board should be organized and used. 

## 6. [Epic](https://gitlab.com/groups/LibreFoodPantry/client-solutions/bear-necessities-market/-/epics/1)
This epic describes the overall plan for the semester. It will be updated as the semester progresses and subepics may be created.  

## 7. [Class Plans](https://gitlab.com/heidijcellis/cs-492-sp-23/-/tree/main/Class%20Plans)
This folder contains information on individual class meetings. **Note that these are updated a week or so ahead of the class meeting and pages for meetings far in the future may contain information from prior offerings of the course.**
