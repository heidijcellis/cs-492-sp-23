# LFP On-Boarding 
You will be working within the LibreFoodPantry community to develop the Bear Necessities Market project.  This activity is intended to be your first introduction to the community and the project. We will start by looking at the larger LibreFoodPantry community as well as gaining an understanding of open source. 

Read the LibreFoodPantry site at https://librefoodpantry.org/ and answer the items below by responding in this document in your own words. 

1. Read the document “Starting an Open Source Project” from the Opensource.guide (https://opensource.guide/starting-a-project/)  project to get a sense of the best practices for starting a new Open Source project. The Starting an Open Source Project identifies four types of documentation that should every open source project have. What are they? 
2. For each of the items identified in number 1, provide the URL for the location of the documentation. If you are unable to locate the relevant documentation indicate this by stating “Unable to find.”
3. What is the license used for the code in LibreFoodPantry? What is the purpose of the open source license? (Why is the software licensed under this license?) 
4. What is the license used for the documentation in LibreFoodPantry? What is the purpose of the documentation license? (Why is the documentation licensed under this license?) 
5. Provide two reasons for having a Contributing guidelines document? 
6. Provide two things that a contributor can do that do not involve coding. 
7. What is the purpose of LibreFoodPantry’s Code of Conduct?
8. Provide one example of an action that could result in a correction from the Code of Conduct. 
9. Provide one example of an action that could result in a warning from the Code of Conduct.
10. What is the source of this Code of Conduct?  
11. What two pledges does the Code of Conduct require of community members? 
12. Who are the community leaders that are responsible for enforcing the Code of Conduct? 
13. As you read the documents for LFP, did you notice any instances of language or tone that are not reflective of the welcoming, inclusive and supportive community that is desired? If so, please point those out here and make suggestions for improvement. Or if you are more comfortable discussing them privately, you may contact any of the community leaders you identified in #12.  
