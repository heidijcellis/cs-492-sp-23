## Agenda Thursday 2/1

* Your group should have the following: 
    * 2-3 epics, one of which should be related to completing the assigned activities and the other related to onboarding
        * Each epic should contain a description of what will be accomplished within that epic
    * Your issue board set up with the columns as described in the [Epics and Issues Activity: ](https://gitlab.com/heidijcellis/cs-492-sp-23/-/blob/main/Activities/Epics%20and%20Issues%20Activity.MD)
        * Note that you do not have to put the results of this activity into your project as the result of the activity itself is the set up of your infrastructure. 
    * At least one project where you will store the results of your activities
    * Somewhere between 8 and 15 issues where:
        * Each issue is related to both an epic and a project. 
        * Note that if you create issues within a project, you can add them to an epic on the right side of the screen. If you create an issue within an epic, you will have to provide the appropriate project. 
* You need to create issues before you start working on the actual activities. 
    * In a professional project, issues are a major form of communication and so you need to let everyone on your team (and outside of your team) know what you're working on.
    * Transparency!!  How will your boss know what you’re doing??
* Labeling of epics and issues:  
    * Both epics and issues need to have a concrete start and end point
    * Epics and issues should not be tasks with no end point such as “learn about GitLab”
    * How do you know that you’re done? How do you know when the issue is ready for review? 
    * If you have something that feels like an ongoing task, break it down

### Next
Planning Meetings
