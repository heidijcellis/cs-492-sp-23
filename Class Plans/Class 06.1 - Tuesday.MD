## Agenda - Tuesday 2/28

### Retrospectives ###
* **Goal:** Improve the team and how it operates
* Meeting involves only team members 
* Everyone read all other team member's retrospective **before the meeting**
* During meeting, discuss the specific questions and how they are answered
* Use the Retrospective format to organize discussion around how to improve team operation
* Come up with one, maybe two, things that team will do to improve how the team works
* Put these in the Working Agreement for each team

### Retrospective Agenda (Times Approximate): ###

**9:30 Meet in Individual Teams**

**10:10 Meet in Larger Teams**
