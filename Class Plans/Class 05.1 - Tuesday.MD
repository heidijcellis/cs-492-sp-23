## Agenda - Tuesday 2/21

### Announcements: ###

* I’ve disconnected the fork of all GuestInfoSystem projects so the GuestInfo team is ready to go
* I’ve also reconfigured Discord so that each major team has a main text and voice team with individual voice and text channels for the individual teams. 
* As of Sunday, it looked like the boards and epics still need to be set up which is fine
* Inventory: Your project should be configured to conform to the current GuestInfoSystem project
* Remember that all “Training” boards must be complete by 5:00 PM Wednesday
    * Make sure that you have addressed all of the comments that I made during the planning meeting and in your grading
* Next Planning Meeting is a week from today
    * By that time, You should have:
        * All boards set up
        * Move Working Working Agreements to new boards and prefix with team name
        * An understanding of which major portions of responsibility will be undertaken by each team
        * One major and 2-4 minor Epics in your project
        * Sub-epics tied to major epics
        * **Let me know if you want feedback on the epics, I’d be happy to take a look and get you feedback before the Planning Meeting**
        * A sprint’s worth of issues in the Sprint Backlog column
        * All issues using the Spike 1.0.0 template
        * All issues are tied to a sub-epic
        * All issues have weights
        * All issue names are prefixed with the team responsible for the issue
        * No need to assign people to the issues
    * Check your issues in the Sprint Backlog to ensure:
        * All issues are clear and measurable – how will you know when they are done? 
        * No duplication or overlap of effort
        * Issues maximize parallelism
        * There are no gaps in coverage of what you are going to do in the sprint 
        * The issues tell a complete story
* Everyone must have their retrospectives done by 5:00 PM Monday 2/27
* Tuesday 2/28 will be Retrospective Meeting – no Stand-ups
* Please start list of questions in your Documentation project 
    * We’ll refine these and send to Kolu 
    * Questions should be done by **5:00 PM Friday 2/24**
    * We’ll send them to Kolu and hopefully get answers by Tuesday or Wednesday
* I’ve revamped the schedule throughout the end of the term
* Teams are on a somewhat staggered schedule so pay attention to due dates'

### Schedule for Stand-ups: ###

**9:45 Front End Fiends**

**10:00 Rubber Ducks**

**10:15 CCLKD**

**10:30 Samurai Commando Squad**

